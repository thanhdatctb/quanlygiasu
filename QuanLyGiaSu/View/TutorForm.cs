﻿using QuanLyGiaSu.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyGiaSu.View
{
    public partial class TutorForm : Form
    {
        private Model1 db = new Model1();
        public TutorForm()
        {
            InitializeComponent();
            this.lblTutorName.Text = Session.MySession.tutor.name;
            this.lblSubject.Text = Session.MySession.tutor.subject;
            this.lblUserName.Text = Session.MySession.tutor.username;
            this.lblExperences.Text = Session.MySession.tutor.experences.ToString() + " tháng";
            //this.dtgvClass.DataSource = new Model1().classes.Where(s => s.turtor == Session.MySession.tutor.id).ToList();
            loadData();
        }
        private void loadData()
        {
            var list = db.classes.Where(s => s.turtor == Session.MySession.tutor.id).ToList();
            BindGrid(list);
        }
        private void BindGrid(List<_class> listStudent)
        {
            dtgvClass.Rows.Clear();
            foreach (var item in listStudent)
            {
                int index = dtgvClass.Rows.Add();
                dtgvClass.Rows[index].Cells[0].Value = item.id;
                dtgvClass.Rows[index].Cells[1].Value = item.student1.username;
                dtgvClass.Rows[index].Cells[2].Value = item.student1.name;
                dtgvClass.Rows[index].Cells[3].Value = item.student1.grade;
                dtgvClass.Rows[index].Cells[4].Value = item.startDate;
                dtgvClass.Rows[index].Cells[5].Value = item.learingDay;
            }
        }
        private void btnLogOut_Click(object sender, EventArgs e)
        {
            Session.MySession.tutor = new Model.tutor();
            this.Close();
            MessageBox.Show("Đăng xuất thành công");
            new LogIn().Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
