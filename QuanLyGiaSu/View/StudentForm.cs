﻿using QuanLyGiaSu.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyGiaSu.View
{
    public partial class StudentForm : Form
    {
        private Model1 db = new Model1();
        public StudentForm()
        {
            InitializeComponent();
            this.lblUserName.Text = Session.MySession.student.username;
            this.lblStudentName.Text = Session.MySession.student.name;
            this.lblGrade.Text = Session.MySession.student.grade.ToString();
            loadData();
        }
        private void loadData()
        {
            var list = db.classes.Where(s => s.student == Session.MySession.student.id).ToList();
            BindGrid(list);
        }
        private void BindGrid(List<_class> listStudent)
        {
            dtgvClass.Rows.Clear();
            foreach (var item in listStudent)
            {
                int index = dtgvClass.Rows.Add();
                dtgvClass.Rows[index].Cells[0].Value = item.id;
                dtgvClass.Rows[index].Cells[1].Value = item.tutor.username;
                dtgvClass.Rows[index].Cells[2].Value = item.tutor.name;
                dtgvClass.Rows[index].Cells[3].Value = item.tutor.subject;
                dtgvClass.Rows[index].Cells[4].Value = item.tutor.experences.ToString()+ " tháng";
                dtgvClass.Rows[index].Cells[5].Value = item.startDate;
                dtgvClass.Rows[index].Cells[6].Value = item.learingDay;
            }
        }
        private void btnLogOut_Click(object sender, EventArgs e)
        {
            Session.MySession.student = new student();
            this.Close();
            MessageBox.Show("Đăng xuất thành công");
            new LogIn().Show();
        }
    }
}
