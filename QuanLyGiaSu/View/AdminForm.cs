﻿using QuanLyGiaSu.Controller;
using QuanLyGiaSu.Model;
using QuanLyThuVien.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyGiaSu.View
{
    public partial class AdminForm : Form
    {
        private StudentController studentController = new StudentController();
        private ClassController classController = new ClassController();
        private TutorController tutorController = new TutorController();
        public AdminForm()
        {
            InitializeComponent();
            this.lblAdminName.Text = Session.MySession.admin.adminName;
            this.lblUserName.Text = Session.MySession.admin.userName;
            LoadData();
            AddBinding();
        }
        private void LoadData()
        {
            this.dtgvTutor.DataSource = this.tutorController.GetAll();
            this.dtgvStudent.DataSource = this.studentController.GetAll();
            this.dtgvClass.DataSource = this.classController.GetAll();
            this.cbTutor.DataSource = this.tutorController.GetAll();
            this.cbTutor.DisplayMember = "name";
            this.cbTutor.ValueMember = "id";
            this.cbStudent.DataSource = this.studentController.GetAll();
            this.cbStudent.DisplayMember = "name";
            this.cbStudent.ValueMember = "id";
        }
        private void AddBinding()
        {
            this.txtStudentId.DataBindings.Add(new Binding("Text", dtgvStudent.DataSource, "id"));
            this.txtStudentName.DataBindings.Add(new Binding("Text", dtgvStudent.DataSource, "name"));
            this.txtStudentUsername.DataBindings.Add(new Binding("Text", dtgvStudent.DataSource, "username"));
            this.txtGrade.DataBindings.Add(new Binding("Text", dtgvStudent.DataSource, "grade"));

            this.txtTutorId.DataBindings.Add(new Binding("Text", dtgvTutor.DataSource, "id"));
            this.txtTutorName.DataBindings.Add(new Binding("Text", dtgvTutor.DataSource, "name"));
            this.txtTutorUsername.DataBindings.Add(new Binding("Text", dtgvTutor.DataSource, "username"));
            this.txtExperence.DataBindings.Add(new Binding("Text", dtgvTutor.DataSource, "experences"));
            this.txtSubject.DataBindings.Add(new Binding("Text", dtgvTutor.DataSource, "subject"));

           this.txtClassId.DataBindings.Add(new Binding("Text", dtgvClass.DataSource,"id"));
           this.cbTutor.DataBindings.Add(new Binding("SelectedValue", dtgvClass.DataSource, "turtor"));
            this.cbStudent.DataBindings.Add(new Binding("SelectedValue", dtgvClass.DataSource, "student"));
            this.dateTimePicker1.DataBindings.Add(new Binding("Value", dtgvClass.DataSource, "startDate"));
            this.textBox1.DataBindings.Add(new Binding("Text", dtgvClass.DataSource, "learingDay"));
        }
        private void btnLogOut_Click(object sender, EventArgs e)
        {
            Session.MySession.admin = new Model.admin();
            this.Close();
            MessageBox.Show("Đăng xuất thành công");
            new LogIn().Show();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            string Username = this.txtStudentUsername.Text;
            string name = this.txtStudentName.Text;
            int grade = int.Parse(this.txtGrade.Text);
            string hashpass = PasswordController.Sha1Hash("admin");
            student student = new student();
            student.grade = grade;
            student.username = Username;
            student.hashpass = hashpass;
            student.status = true;
            student.name = name;
            try
            {
                studentController.Add(student);
                new AdminForm().Show();
                MessageBox.Show("Thêm học sinh thành công");
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int id = int.Parse(this.txtStudentId.Text);
            string Username = this.txtStudentUsername.Text;
            string name = this.txtStudentName.Text;
            int grade = int.Parse(this.txtGrade.Text);
            string hashpass = this.studentController.FindById(id).hashpass ;
            student student = new student();
            student.id = id;
            student.grade = grade;
            student.username = Username;
            student.hashpass = hashpass;
            student.status = true;
            student.name = name;
            try
            {
                studentController.Edit(student);
                new AdminForm().Show();
                MessageBox.Show("Sửa học sinh thành công");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int id = int.Parse(this.txtStudentId.Text);
            string Username = this.txtStudentUsername.Text;
            string name = this.txtStudentName.Text;
            int grade = int.Parse(this.txtGrade.Text);
            string hashpass = this.studentController.FindById(id).hashpass;
            student student = new student();
            student.grade = grade;
            student.username = Username;
            student.hashpass = hashpass;
            student.status = true;
            student.name = name;
            try
            {
                studentController.Delete(id);
                new AdminForm().Show();
                MessageBox.Show("Xóa học sinh thành công");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //int id = int.Parse(this.txtTutorId.Text);
            string name = this.txtTutorName.Text;
            string hashpass = PasswordController.Sha1Hash("admin");
            string username  = this.txtTutorUsername.Text;
            int experences = int.Parse(this.txtExperence.Text);
            string subject = this.txtSubject.Text;
            tutor tutor = new tutor();
            tutor.name = name;
            tutor.hashpass = hashpass;
            tutor.username = username;
            tutor.experences = experences;
            tutor.status = true;
            tutor.subject = subject;
            try
            {
                if (this.tutorController.Add(tutor))
                {
                    MessageBox.Show("Thêm gia sư thành công");
                    new AdminForm().Show();
                }

            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            _class _Class = new _class();
            _Class.startDate = this.dateTimePicker1.Value;
            //_Class.id = int.Parse(this.txtClassId.Text);
            _Class.student = (int)this.cbStudent.SelectedValue;
            _Class.turtor = (int)this.cbTutor.SelectedValue;
            _Class.learingDay = this.textBox1.Text;
            _Class.status = true;
            try
            {
                if (this.classController.Add(_Class))
                {
                    MessageBox.Show("Thêm lớp thành công");
                    new AdminForm().Show();
                }
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            int id = int.Parse(this.txtTutorId.Text);
            string name = this.txtTutorName.Text;
            string hashpass = PasswordController.Sha1Hash("admin");
            string username = this.txtTutorUsername.Text;
            int experences = int.Parse(this.txtExperence.Text);
            string subject = this.txtSubject.Text;
            tutor tutor = new tutor();
            tutor.id = id;
            tutor.name = name;
            tutor.hashpass = hashpass;
            tutor.username = username;
            tutor.experences = experences;
            tutor.status = true;
            tutor.subject = subject;
            try
            {
                if (this.tutorController.Edit(tutor))
                {
                    MessageBox.Show("Sửa gia sư thành công");
                    new AdminForm().Show();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int id = int.Parse(this.txtTutorId.Text);
            string name = this.txtTutorName.Text;
            string hashpass = PasswordController.Sha1Hash("admin");
            string username = this.txtTutorUsername.Text;
            int experences = int.Parse(this.txtExperence.Text);
            string subject = this.txtSubject.Text;
            tutor tutor = new tutor();
            tutor.name = name;
            tutor.hashpass = hashpass;
            tutor.username = username;
            tutor.experences = experences;
            tutor.status = true;
            tutor.subject = subject;
            try
            {
                if (this.tutorController.Delete(id))
                {
                    MessageBox.Show("Xóa gia sư thành công");
                    new AdminForm().Show();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            _class _Class = new _class();
            _Class.startDate = this.dateTimePicker1.Value;
            _Class.id = int.Parse(this.txtClassId.Text);
            _Class.student = (int)this.cbStudent.SelectedValue;
            _Class.turtor = (int)this.cbTutor.SelectedValue;
            _Class.learingDay = this.textBox1.Text;
            _Class.status = true;
            try
            {
                if (this.classController.Edit(_Class))
                {
                    MessageBox.Show("Sửa lớp thành công");
                    new AdminForm().Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            _class _Class = new _class();
            _Class.startDate = this.dateTimePicker1.Value;
            _Class.id = int.Parse(this.txtClassId.Text);
            _Class.student = (int)this.cbStudent.SelectedValue;
            _Class.turtor = (int)this.cbTutor.SelectedValue;
            _Class.learingDay = this.textBox1.Text;
            _Class.status = true;
            try
            {
                if (this.classController.Delete(_Class.id))
                {
                    MessageBox.Show("Xóa lớp thành công");
                    new AdminForm().Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
