namespace QuanLyGiaSu.Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model11")
        {
        }

        public virtual DbSet<admin> admins { get; set; }
        public virtual DbSet<_class> classes { get; set; }
        public virtual DbSet<student> students { get; set; }
        public virtual DbSet<tutor> tutors { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<admin>()
                .Property(e => e.userName)
                .IsUnicode(false);

            modelBuilder.Entity<admin>()
                .Property(e => e.hashpass)
                .IsUnicode(false);

            modelBuilder.Entity<student>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<student>()
                .Property(e => e.hashpass)
                .IsUnicode(false);

            modelBuilder.Entity<student>()
                .HasMany(e => e.classes)
                .WithOptional(e => e.student1)
                .HasForeignKey(e => e.student);

            modelBuilder.Entity<tutor>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<tutor>()
                .Property(e => e.hashpass)
                .IsUnicode(false);

            modelBuilder.Entity<tutor>()
                .HasMany(e => e.classes)
                .WithOptional(e => e.tutor)
                .HasForeignKey(e => e.turtor);
        }
    }
}
