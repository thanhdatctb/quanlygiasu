namespace QuanLyGiaSu.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("class")]
    public partial class _class
    {
        public int id { get; set; }

        public int? student { get; set; }

        public int? turtor { get; set; }

        [Column(TypeName = "date")]
        public DateTime? startDate { get; set; }

        [StringLength(20)]
        public string learingDay { get; set; }

        public bool? status { get; set; }

        public virtual student student1 { get; set; }

        public virtual tutor tutor { get; set; }
    }
}
