namespace QuanLyGiaSu.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tutor")]
    public partial class tutor
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tutor()
        {
            classes = new HashSet<_class>();
        }

        public int id { get; set; }

        [StringLength(100)]
        public string username { get; set; }

        [StringLength(100)]
        public string hashpass { get; set; }

        [StringLength(100)]
        public string name { get; set; }

        public int? experences { get; set; }

        [StringLength(100)]
        public string subject { get; set; }

        public bool? status { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<_class> classes { get; set; }
    }
}
