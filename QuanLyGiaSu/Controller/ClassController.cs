﻿using QuanLyGiaSu.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyGiaSu.Controller
{
    class ClassController
    {
        Model1 db = new Model1();
       public List<_class> GetAll()
        {
            return db.classes.Where(s => s.status == true).ToList();
        }
        public bool Add(_class student)
        {
            db.classes.Add(student);
            db.SaveChanges();
            return true;
        }
        public bool Edit(_class student)
        {
            //this.db.Entry(student).State = EntityState.Modified;
            db.classes.AddOrUpdate(student);
            //student updatestd = db.students.Find(student.id);
            //updatestd.name = student.name;
            db.SaveChanges();
            return true;
        }
        public bool Delete(int id)
        {
            _class cat = this.db.classes.Find(id);
            cat.status = false;

            db.Entry(cat).State = EntityState.Modified;
            db.SaveChanges();

            return true;
        }
        public student FindById(int id)
        {
            return db.students.Find(id);
        }
    }
}
