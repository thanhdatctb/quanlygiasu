﻿using QuanLyGiaSu.Model;
using QuanLyThuVien.Controller;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyGiaSu.Controller
{
    class TutorController
    {

        private Model1 db = new Model1();
        public bool LogIn(string username, string password)
        {
            string hashpass = PasswordController.Sha1Hash(password);
            var tutor = (tutor)db.tutors.Where(e => e.username == username).Where(e => e.hashpass == hashpass).FirstOrDefault();
            if (tutor != null)
            {
                Session.MySession.tutor = tutor;
                return true;
            }
            return false;
        }
        public List<tutor> GetAll()
        {
            return db.tutors.Where(s => s.status == true).ToList();
        }
        public bool Add(tutor student)
        {
            db.tutors.Add(student);
            db.SaveChanges();
            return true;
        }
        public bool Edit(tutor student)
        {
            //this.db.Entry(student).State = EntityState.Modified;
            db.tutors.AddOrUpdate(student);
            //student updatestd = db.students.Find(student.id);
            //updatestd.name = student.name;
            db.SaveChanges();
            return true;
        }
        public bool Delete(int id)
        {
            tutor cat = this.db.tutors.Find(id);
            cat.status = false;

            db.Entry(cat).State = EntityState.Modified;
            db.SaveChanges();

            return true;
        }
        public student FindById(int id)
        {
            return db.students.Find(id);
        }
    }
}
