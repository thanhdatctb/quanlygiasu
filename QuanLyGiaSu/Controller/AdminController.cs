﻿using QuanLyGiaSu.Model;
using QuanLyThuVien.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyGiaSu.Controller
{
    class AdminController
    {

        private Model1 db = new Model1();
        public bool LogIn(string username, string password)
        {
            string hashpass = PasswordController.Sha1Hash(password);
            var student = (admin)db.admins.Where(e => e.userName == username).Where(e => e.hashpass == hashpass).FirstOrDefault();
            if (student != null)
            {
                Session.MySession.admin = student;
                return true;
            }
            return false;
        }
    }
}
