﻿using QuanLyGiaSu.Model;
using QuanLyThuVien.Controller;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyGiaSu.Controller
{
    class StudentController
    {
        private Model1 db = new Model1();
        public bool LogIn(string username, string password)
        {
            string hashpass = PasswordController.Sha1Hash(password);
            var student = (student) db.students.Where(e => e.username == username).Where(e => e.hashpass == hashpass).FirstOrDefault();
            if(student != null)
            {
                Session.MySession.student = student;
                return true;
            }
            return false;
        }
        public List<student> GetAll()
        {
            return db.students.Where(s => s.status == true).ToList();
        }
        public bool Add(student student)
        {
            db.students.Add(student);
            db.SaveChanges();
            return true;
        }
        public bool Edit(student student)
        {
            //this.db.Entry(student).State = EntityState.Modified;
            db.students.AddOrUpdate(student);
            //student updatestd = db.students.Find(student.id);
            //updatestd.name = student.name;
            db.SaveChanges();
            return true;
        }
        public bool Delete(int id)
        {
            student cat = this.db.students.Find(id);
            cat.status = false;
            
            db.Entry(cat).State = EntityState.Modified;
            db.SaveChanges();

            return true;
        }
        public student FindById(int id)
        {
            return db.students.Find(id);
        }
    }
}
