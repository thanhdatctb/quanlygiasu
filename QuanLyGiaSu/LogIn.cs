﻿using QuanLyGiaSu.Controller;
using QuanLyGiaSu.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyGiaSu
{
  
    public partial class LogIn : Form
    {
        private StudentController studentController = new StudentController();
        private TutorController TutorController = new TutorController();
        private AdminController AdminController = new AdminController();
        public LogIn()
        {
            InitializeComponent();
        }

        private void btnLogIn_Click(object sender, EventArgs e)
        {
            string username = this.txtUsername.Text;
            string pass = this.txtPassword.Text;
            try
            {
                if (studentController.LogIn(username, pass))
                {
                    MessageBox.Show("Đăng nhập "+Session.MySession.student.name);
                    new StudentForm().Show();
                }
                else if (AdminController.LogIn(username, pass))
                {
                    MessageBox.Show("Đăng nhập " + Session.MySession.admin.adminName);
                    new AdminForm().Show();
                }
                else if(TutorController.LogIn(username,pass))
                {
                    MessageBox.Show("Đăng nhập " + Session.MySession.tutor.name);
                    new TutorForm().Show();
                }    
                else
                {
                    MessageBox.Show("Tên đăng nhập hoặc mật khẩu không đúng");
                }    
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
