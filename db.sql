﻿use master
go 
use master
go
if (exists(select * from sysdatabases where name = 'QuanLyGiaSu'))
drop database QuanLyGiaSu
go
create database QuanLyGiaSu
go 
use QuanLyGiaSu
go
create table admin
(
	id int primary key identity(1,1),
	userName varchar(100),
	hashpass varchar(100),
	adminName nvarchar(100)
)
go
create table tutor
(
	id int primary key identity(1,1),
	username varchar(100),
	hashpass varchar(100),
	name nvarchar(100),
	experences int,
	subject nvarchar(100),
	status bit default 1
)
go
create table student
(
	id int primary key identity(1,1),
	username varchar(100),
	hashpass varchar(100),
	name nvarchar(100),
	grade int,
	status bit default 1
)
go
create table Tutorclass
(
	id int primary key identity(1,1),
	student int foreign key references student(id),
	turtor int foreign key references tutor(id),
	startDate date ,
	learingDay nvarchar(20) ,
	status bit default 1
)
go
---username: admin, password: admin, hash: sha1
insert into admin (userName,hashpass,adminName)values 
( 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Admin'),
('admin2', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Admin2')
go
insert into tutor (username,hashpass ,	name ,experences ,subject) values
('hungnv', 'd033e22ae348aeb5660fc2140aec35850c4da997', N'Nguyễn Văn Hùng', 12, N'Toán' ),
('hant', 'd033e22ae348aeb5660fc2140aec35850c4da997', N'Nguyễn Thị Hà', 12, N'Địa' )
go
insert into student (username,hashpass,name,grade ) values
('sontv', 'd033e22ae348aeb5660fc2140aec35850c4da997', N'Trần Văn Sơn', 10),
('navt', 'd033e22ae348aeb5660fc2140aec35850c4da997', N'Vũ Thị Na', 12)
go
insert into Tutorclass (student ,turtor,startDate ,learingDay) values
(1,1,'10-24-2020',N'Thứ 2'),
(1,2,'10-10-2020',N'Thứ 3'),
(2,1,'11-10-2020',N'Thứ 4'),
(2,2,'11-10-2020',N'Thứ 5')

select * from student